package com.demo.springboot.domain.dto;

import java.io.Serializable;

public class MovieDto implements Serializable {
    private String movieId;
    private String title;
    private String year;
    private String image;

    public MovieDto(String movieId, String title, String year, String image) {
        this.movieId = movieId;
        this.title = title;
        this.year = year;
        this.image = image;
    }

    public String getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public String getImage() {
        return image;
    }
}