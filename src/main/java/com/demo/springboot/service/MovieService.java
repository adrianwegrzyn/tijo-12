package com.demo.springboot.service;

import com.demo.springboot.domain.dto.MovieDto;
import java.util.ArrayList;

public interface MovieService {
    ArrayList<MovieDto> getMovieFromCsv();
}
