package com.demo.springboot.service;

import com.demo.springboot.domain.dto.MovieDto;
import org.springframework.stereotype.Service;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

@Service
public class MovieServiceImpl implements MovieService {

    private final ArrayList<MovieDto> movieList = new ArrayList<>();

    @Override
    public ArrayList<MovieDto> getMovieFromCsv() {

        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";
        String csvFile = "./src/main/resources/movies.csv";
        try {
            movieList.clear();
            br = new BufferedReader(new FileReader(csvFile));

            while ((line = br.readLine()) != null) {
                String[] movie = line.split(cvsSplitBy);
                try{
                    Integer.parseInt(movie[0]);
                }catch (Exception e){
                    continue;
                }
                movieList.add(new MovieDto(movie[0], movie[1], movie[2], movie[3]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return movieList;
    }
}
